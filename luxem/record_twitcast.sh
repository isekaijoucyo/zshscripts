#!/bin/bash
# TwitCasting Live Stream Recorder

if [[ ! -n "$1" ]]; then
  echo "usage: $0 twitcasting_id [interval] [immediate:t]"
  exit 1
fi

log_prefix() {
  echo -n $(date +"[%Y-%m-%d %H:%M:%S]")
}

# default interval set to 60 (seconds)
INTERVAL="${2:-60}"

# default immediate mode set to 1(false), if $3 == "t" then mode is set to 0(true)
if [[ -z "$3" ]] || [[ "$3" != "t" ]]; then
  IS_IMMEDIATE=false
else
  IS_IMMEDIATE=true
fi

while true; do
  # Monitor live streams of specific user
  while true; do
    LOG_PREFIX=$(log_prefix)
    echo "$LOG_PREFIX Try to get current live stream of twitcasting.tv/$1"
    STREAM_API="https://twitcasting.tv/streamserver.php?target=$1&mode=client"
    (curl -s "$STREAM_API" | grep -q '"live":true') && break

    echo "$LOG_PREFIX The stream is not available now."
    echo "$LOG_PREFIX Retry after $INTERVAL seconds..."
    sleep $INTERVAL
  done

  # Record using MPEG-2 TS format to avoid broken file caused by interruption
  FNAME="twitcast_${1}_$(date +"%Y%m%d_%H%M%S").ts"
  echo "$LOG_PREFIX Start recording, stream saved to \"$FNAME\"."

  # Also record low resolution stream simultaneously as backup
  # The following part might be broken for now!!
  #M3U8_URL="http://twitcasting.tv/$1/metastream.m3u8?video=1"
  #ffmpeg -i "$M3U8_URL" -codec copy -f mpegts "$FNAME" > "$FNAME.log" 2>&1 &

  # Start recording
  RECORD_TIMESTAMP_START=$(date +%s)
  STREAMLINK_OUTPUT=$(streamlink --http-timeout 4 --stream-segment-timeout 6 --stream-timeout 6 "https://twitcasting.tv/$1" best -o $1_$(date +"%Y%m%d_%H%M%S").streamlink.ts 2>&1)
  # Check the streamlink output, if the output indicates that the current live is membership, 
  #  pause the monitor process for a while.
  RECORD_TIMESTAMP_END=$(date +%s)
  echo "DEBUG: $RECORD_TIMESTAMP_END - $RECORD_TIMESTAMP_START gap: $(($RECORD_TIMESTAMP_END - $RECORD_TIMESTAMP_START))"
  echo "DEBUG: LOG: $STREAMLINK_OUTPUT"
  echo "=============================="
  MEMBERSHIP_RET1=$(echo $STREAMLINK_OUTPUT | grep "^\[cli\]\[info\] Writing output to$")
  MEMBERSHIP_RET2=$(echo $STREAMLINK_OUTPUT | grep "^\[cli\]\[error\].*Could not open stream.*TwitCastingStream")
  GAP=$((RECORD_TIMESTAMP_END - RECORD_TIMESTAMP_START))
  if [[ $GAP -le 20 ]] && [[ $MEMBERSHIP_RET1 -eq 0 ]] && [[ $MEMBERSHIP_RET2 -eq 0 ]]; then
    # This might be membership live, sleep for another extra 20 minutes
    echo "$(log_prefix) Cooldown set to 20 minutes for a potential membership livestream."
    sleep 20m && continue
  fi

  # Retry immediately for several times for new streamers (1 waku limited to 30 minutes)
  # Not checking potential membership status
  RETRIES_FAST=3
  RETRIES_SLOW=5
  RETRY_FAST_INTERVAL=0.5
  RETRY_SLOW_INTERVAL=8
  if $IS_IMMEDIATE; then
    for i in {1..$RETRIES_FAST}; do
      streamlink --http-timeout 2 --stream-segment-timeout 3 --stream-timeout 3 "https://twitcasting.tv/$1" best -o $1_$(date +"%Y%m%d_%H%M%S").streamlink.ts 2>&1
      sleep $RETRY_FAST_INTERVAL
    done
    echo "DEBUG: fast retries done."
    for j in {1..$RETRIES_SLOW}; do
      streamlink --http-timeout 4 --stream-segment-timeout 6 --stream-timeout 6 "https://twitcasting.tv/$1" best -o $1_$(date +"%Y%m%d_%H%M%S").streamlink.ts 2>&1
      sleep $RETRY_SLOW_INTERVAL
    done
    echo "DEBUG: slow retries done."
  fi

  # Exit if we just need to record current stream
  LOG_PREFIX=$(log_prefix)
  echo "$LOG_PREFIX Live stream recording stopped. Immediate retries done."
done
