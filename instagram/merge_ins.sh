#!/usr/bin/env zsh

m4a_count=`ls m4a/*m4a* | wc -l`
m4v_count=`ls m4v/*m4v* | wc -l`
merge_script="$0"
finish () {
	if [[ -L "$merge_script" && -f "$merge_script" ]]; then
		\rm -v "$merge_script"
	fi
}

check_m4av () {
	if [[ $m4a_count -ne $m4v_count && $m4v_count -ne 0 ]]; then
		echo "m4a($m4a_count) m4v($m4v_count) count mismatch!" 2>&1
	       	exit 1
	elif [[ $m4v_count -eq 0 ]]; then
		echo "m4a only count: $m4a_count"
		return 1
	else
		echo "m4a/m4v count: $m4a_count"
		return 0
	fi
}

prepare_init_m4av () {
	init_m4a="$(find . -maxdepth 1 -name "*_0-init.m4a?m" | tail -1)" && init_m4a=${init_m4a#./}
	if [[ $init_m4a == "" ]]; then
		echo "init_m4a not detected." 2>&1
		exit 1
	fi
	\cp -fv "$init_m4a" ./_init.m4a
	if [[ -z $1 || $1 -eq 0 ]]; then
		init_m4v="$(find . -maxdepth 1 -name "*_0-init.m4v?m" | tail -1)" && init_m4v=${init_m4v#./}
		if [[ $init_m4v == "" ]]; then
			echo "initm4v not found" 2>&1
			exit 1
		fi
		\cp -fv "$init_m4v" ./_init.m4v
	fi
}

concat_m4av () {
	concat_count=1
	for m4a in `ls -v m4a/*m4a?m`; do
		echo -ne "Concating: ($concat_count/$m4a_count)\r"
		cat "$m4a" >> ./_init.m4a

		if [[ -z $1 || $1 -eq 0 ]]; then
			m4v=$(echo $m4a | sed -e 's/^m4a/m4v/' -e 's/m4a?m$/m4v?m/')
			cat "$m4v" >> ./_init.m4v
		fi
		concat_count=$((concat_count+1))
	done
}

merge_m4av () {
	final_mp4="$(basename "$(pwd)")_instagram.mp4"
	find . -maxdepth 1 -name "$final_mp4" -delete

	if [[ -z $1 || $1 -eq 0 ]]; then
		ffmpeg -y -hide_banner -loglevel warning -i _init.m4v -i _init.m4a -c copy "$final_mp4"
	else
		ffmpeg -y -hide_banner -loglevel warning -i _init.m4a -c copy "$final_mp4"
	fi

	ffprobe -hide_banner "$final_mp4"
	ret=$?
	if [[ $ret -eq 0 ]]; then
		\rm -vf ./_init.m4a ./_init.m4v
	else
		echo "ffprobe retval: $ret" 2>&1
		exit 1
	fi
}

cleanup_and_pack_misc () {
	# clean up
	\rm -r ./m4a
	[[ -z $1 || $1 -eq 0 ]] && \rm -r ./m4v

	# pack misc
	mkdir -p misc/
	mv -vn *mpd* *m4a?m diffout url misc/
	if [[ $? -ne 0 ]]; then
		echo "cleanup: move m4a to misc failed!" 2>$1
		exit 1
	fi
	[[ -z $1 || $1 -eq 0 ]] && mv -vn *.m4v?m misc/
	if [[ $? -ne 0 ]]; then
		echo "cleanup: move m4v to misc failed!" 2>$1
		exit 1
	fi
	tar cvfz misc.tar.gz misc
	if [[ $? -ne 0 ]]; then
		echo "cleanup: tar misc.tar.gz failed!" 2>$1
		exit 1
	fi
	\rm -r misc/
}

check_m4av
m4av_type=$?

prepare_init_m4av $m4av_type
concat_m4av $m4av_type
merge_m4av $m4av_type
cleanup_and_pack_misc $m4av_type

# self destruction if this script file is a symlink
#trap finish EXIT
finish
