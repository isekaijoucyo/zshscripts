CREATE TABLE IF NOT EXISTS "Record" (
        "ID"    INTEGER,
        "Type"  INTEGER,
        "Url"   TEXT,
        "Name"  TEXT,
        "Localrepo"     INTEGER,
        "Remoterepo"    INTEGER,
        "Recordmtime"   INTEGER,
        "Uploadtime"    INTEGER,
        "Filesize"      INTEGER,
        "MD5"   TEXT,
        "Status"        INTEGER,
        "Description"   TEXT,
        PRIMARY KEY("ID" AUTOINCREMENT)
);
CREATE TABLE sqlite_sequence(name,seq);
