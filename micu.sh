#!/usr/bin/env zsh

record_file="record.txt"
remote_host="micu"
subdir="youtube"
logfile="micu.$(date +%Y%m%d).log"

if [[ $# -eq 1 ]] || [[ $# -eq 2 ]]; then
    keyword=$1

    # Check if the record file exists
    if [[ -f $record_file ]]; then
        # Check if the keyword exists in the record file
        if grep -q "^${keyword}=" $record_file; then
            directory=$(grep "^${keyword}=" $record_file | awk -F'=' '{print $2}')
            echo "Directory for ${keyword}: ${directory}"
        else
            if [[ $# -eq 2 ]]; then
                directory=$2
                echo "${keyword}=${directory}" >> $record_file
            else
                echo "Channel name not found. Please provide a directory for ${keyword}:"
                # $directory without spaces
                read directory
                echo "${keyword}=${directory}" >> $record_file
            fi
            echo "New entry (${keyword}: ${directory})  added to the record."
        fi
        mkdir -p ${directory}/${subdir}/
        for f in *"${keyword}"*.mp4 ;do
            ff="${f%%.mp4}"
            ff="${ff#*-}"
            mv -vn -- *"$ff"* "$directory"/"$subdir"/
        done
	echo rclone copy ${directory} | tee -a "$logfile"
        rclone copy "${directory}" "${remote_host}:/${directory}" -v -P --log-file "$logfile"
        rclone_ret=$?
	echo "rclone done retval($rclone_ret)" | tee -a "$logfile"
        # purge the ${directory} if rclone copy succeed
        if [[ ${rclone_ret} -eq 0 ]]; then
            purge "${directory}"
        fi
    else
        echo "Record file not found. Please run the maintenance script first."
    fi
else
    echo "Usage: $0 <keyword>"
fi
