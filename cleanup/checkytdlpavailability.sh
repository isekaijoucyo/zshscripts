#!/usr/bin/env zsh

total_count=0

# Reffering to discussion on youtube id format as below:
# 1. https://stackoverflow.com/questions/6180138/whats-the-maximum-length-of-a-youtube-video-id
# 2. https://stackoverflow.com/questions/2742813/how-to-validate-youtube-video-ids
# * https://webapps.stackexchange.com/questions/54443/format-for-id-of-youtube-video/101153#101153
is_youtube_vid () {
  (echo "$1" | grep -Eq  ^[0-9A-Za-z_-]{10}[048AEIMQUYcgkosw]$) && echo "0" || echo "1"
}
YTDLP_YOUTUBE_RE_SUFFIX='[0-9A-Za-z_-]{10}[048AEIMQUYcgkosw].\mp4'

# $1: filename
show_channel_name () {
  ffprobe -hide_banner "$1" -show_entries "format_tags=artist" 2>/dev/null | tail -n +2 | head -n -1 | sed -E 's/^TAG:artist=//'
  ret=$?
  return $ret
}

# $1: ytid (youtube id)
# retval: 0: availiable, 9: wrong input
check_youtube_id_availibility () {
  [[ -z $1 ]] && return 9
  ytid="$1"
  # attemption on fetching the video name and other metadata
  result=$(yt-dlp --simulate --print "%(duration>%H:%M:%S.%s)s %(upload_date>%Y-%m-%d)s %(id)s" "$ytid" 2>&1)
  if echo "$result" | grep -q "^ERROR"; then
    # the ytid video is inaccessible on youtube, keep this video in my backup instead.
    echo "${ytid} KEEP1"
  else
    # split the result and extract the upload_date info
    result_arr=($=result)
    # if the upload_date earlier than 1 month ago...
    if [[ $(date -d "$result[2]" +%s) -gt $(date -d "+1 month ago" +%s) ]]; then
    # the ytid video is accessible, but within a month, roughly. Keep this video in my backup in case it is removed later on.
      echo "${ytid} KEEP2"
    else
      echo "${ytid} REMOVE"
    fi
  fi
  return 0
}

# find valid yt-dlp youtube streaming archives under the current directory
# bash readarray syntax
# readarray -d '' valid_files < <(find . -maxdepth 1 -regextype egrep -regex '.*-[0-9A-Za-z_-]{10}[048AEIMQUYcgkosw]\.mp4' -print0 -printf "%P\n")
valid_files=( "${(@0)$(find . -maxdepth 1 -regextype egrep -regex '.*-[0-9A-Za-z_-]{10}[048AEIMQUYcgkosw]\.mp4' -printf "%P\\0")%?}" )

for ((i=1; i<=$#valid_files; i++)); do
  #echo "[$((i-1))] ${valid_files[i]}"
  show_channel_name "${valid_files[i]}"
  ytid=`echo ${valid_files[i]} | sed -nE 's/.*-([A-Za-z0-9_-]{11})\.mp4/\1/p'`
  check_youtube_id_availibility $ytid
done
