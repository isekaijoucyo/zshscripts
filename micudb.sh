#!/usr/bin/env zsh

record_db="micu.db"
remote_host="micu"
subdir="youtube"

if [[ $# -eq 1 ]]; then
else
    echo "Usage: $0 <record>"
fi

reconstruct_string() {
    local reconstructed_string=""
    local text_indicies=(2 3 9 11)
    # seperate the input string by delimiter ';'
    array_value=("${(@s[;])1}")
    # Loop through each element in the array
    ##for element in "${@}"; do
    ##    # set empty element as NULL
    ##    if [[ -z "$element" ]]; then
    ##        formatted_element="NULL"
    ##    # Check if the element contains commas or colons or spaces
    ##    elif [[ "$element" =~ ",|:| " ]]; then
    ##        # Quote the element with single quotes
    ##        formatted_element="'${element}'"
    ##    else
    ##        # No commas, use element as is
    ##        formatted_element="$element"
    ##    fi
    ##    # Add the element to the reconstructed string
    ##    reconstructed_string="${reconstructed_string}${formatted_element},"
    ##done
    #print -rl -- "$array_value[@]"
    for ((i = 1; i <= ${#array_value[@]}; i++)); do
        # set empty element as NULL
        element="$array_value[$i]"
        if [[ -z "${element}" ]]; then
            formatted_element="NULL"
        # single quote up the TEXT columns which are not NULL
        elif [[ "${text_indicies[@]}" =~ "$i" ]]; then
            formatted_element="'${element}'"
        else
            formatted_element="${element}"
        fi
        reconstructed_string="${reconstructed_string}${formatted_element},"
    done

    # Remove the trailing comma from the last element
    reconstructed_string="${reconstructed_string%,}"

    # Print or return the reconstructed string
    echo "$reconstructed_string"
}

# full sql :query: <db> <cmd1> <table> <column_format> <value>
# record_query <cmd1> <value>
record_query () {
    # check the numbers if arguments
    if [[ $# -ne 2 ]]; then
        echo "Error: Function requires 2 arguments." >&2
        return 1
    fi

    # check supported queries
    cmd1="$1"
    if [[ "$cmd1" != "INSERT INTO" ]]; then
        echo "Error: query command not supported." >&2
        return 1
    fi

    # e.g. Record (Type, Url, Name, Localrepo, Remoterepo, Recordmtime, Uploadtime, Filesize, MD5, Status, Description)

    # columns: ('$type', '$url', '$name', $localrepo, $remoterepo, $recordmtime, $uploadtime, $filesize, '$md5', $status, '$description')
    # e.g. "0; https://youtu.be/tT1ZxBAtw9M; LASAGNA IN MICROWAVE OVEN | LASAGNA RECIPE | lutong pinoy ni oyoboy; \
    #       2; 2; 1702092634; 1702092640; 4096; f447b20a7fcbf53a5d5be013ea0b15af; 0; Hello world"

    value="$2"
    # format the value and assign it to an array
    formatted_value=$(echo "$value" | sed 's/[ ]*;[ ]*/;/g')
    #adjusted_value=$(reconstruct_string "${array_value[@]}")
    reconstruct_string "${formatted_value}"
    adjusted_value=$(reconstruct_string "${formatted_value}")

    statement="$cmd1 Record(Type, Url, Name, Localrepo, Remoterepo, Recordmtime, Uploadtime, Filesize, MD5, Status, Description) VALUES($adjusted_value);"
    echo $statement
    echo "============"
    sleep 10

    # execute query
    # EXECUTE insert_record("$type", "$url", "$name", $localrepo, $remoterepo, $recordmtime, $uploadtime, $filesize, "$md5", $status, "$description");
    #PREPARE insert_record(Type, Url, Name, Localrepo, Remoterepo, Recordmtime, Uploadtime, Filesize, MD5, Status, Description) AS 
    #INSERT INTO Record (Type, Url, Name, Localrepo, Remoterepo, Recordmtime, Uploadtime, Filesize, MD5, Status, Description) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
    #insert_record("$adjusted_value");
    sqlite3 "${record_db}" <<-EOF
    BEGIN TRANSACTION;

    ${statement}
    COMMIT;
EOF
    # collect sqlite3 return value
    sql_retval=$?
    if [[ $sql_retval -ne 0 ]]; then
      echo "Error: sqlite3 transaction failed with code: $sql_retval" >&2
      return $sql_retval
    fi
    return 0
}

record_query "$1" "$2"
