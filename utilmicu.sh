#!/usr/bin/env zsh

record_file="record.txt"
remote_host="micu"
subdir="youtube"

# Function to check if a given item is in list.txt
check() {
    local item=$1
    # Check if the keyword exists in the record file
    if grep -q "^${item}=" $record_file; then
        directory=$(grep "^${item}=" $record_file | awk -F'=' '{print $2}')
        echo "Record hit: \"$item: ${directory}\"."
        exit 0
    else
        echo "$item is not in the record file."
        exit 1
    fi
}

# Function to sort list.txt alphanumerically
sort_list() {
    tmp_file=".${record_file}.$(date +%s)"
    sort -o "$record_file" $tmp_file
    if [[ "$(cat $record_file | wc -l)" -eq "$(cat $tmp_file | wc -l)" ]]; then
      \rm -v $record_file
      \mv -v $tmp_file $record_file
    fi
    echo "${record_file} has been sorted alphanumerically."
}

# Check if the correct number of arguments is provided
if [[ $# -lt 1 ]]; then
    echo "Usage: $0 <subcommand>"
    exit 1
fi

# Determine the subcommand and execute the corresponding function
case $1 in
    check)
        if [[ $# -lt 2 ]]; then
            echo "Usage: $0 check <item>"
            exit 1
        fi
        check $2
        ;;
    sort)
        sort_list
        ;;
    *)
        echo "Invalid subcommand: $1"
        echo "Usage: $0 <check|sort>"
        exit 1
        ;;
esac

