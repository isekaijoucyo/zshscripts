#!/usr/bin/env zsh

record_file="record.txt"

if [[ $# -eq 2 ]]; then
    # Check if the record file exists
    if [[ ! -f $record_file ]]; then
        touch $record_file
    fi

    channel_name=$1
    directory=$2

    # Check if the channel name already exists in the record file
    if grep -q "^${channel_name}=" $record_file; then
        echo "Channel name already exists. Updating directory."
        sed -i "s|^${channel_name}=.*|${channel_name}=${directory}|g" $record_file
    else
        echo "${channel_name}=${directory}" >> $record_file
        echo "New entry added to the record."
    fi
else
    echo "Usage: $0 <channel_name> <directory>"
fi

