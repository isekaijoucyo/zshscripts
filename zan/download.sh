#!/usr/bin/env zsh

# TODO check zsh availibity
# download zan archive from info.txt, netscape cookie file location: ~/zancook
# TODO using $1 as file name for now
filename="$1"
echo "File name: $filename"

downloadcmts() {
	mkdir -p cmts/
	# 4th line: cmts url
	# use ~/zshscripts/zan/cmtsa.sh to download comments
	cmts_url=$(sed -n '4p;' info.txt)
	# cleanup previous files
	find cmts/ -type f -delete
	\rm -f cmts.tar.gz
	cd cmts
	wget "$cmts_url"
	index_file=`find . -name "*index*json" -type f | tail -1`
	~/zshscripts/zan/cmtsa.sh "$index_file"
	retval="$?"
	if [[ $retval -eq 0 ]]; then
		cd ..
		tar cvfz cmts.tar.gz cmts/
		if [[ $? -ne 0 ]]; then echo "abort while tarballing cmts/. ret:$retval" 2>&1 && exit 1; fi
		#\rm -r cmts/
	else
		echo "abort while downloading comments. ret:$retval" 2>&1
		exit 1
	fi
}

downloadm3u8() {
	mkdir -p m3u8/
	# 3rd line: m3u8 url
	m3u8_url=$(sed -n '3p;' info.txt)
	# cleanup previous files
	find m3u8/ -type f -delete
	\rm -f m3u8.tar.gz
	cd m3u8
	wget "$m3u8_url"
	cat index.m3u8 | grep ".*\.m3u8$" | xargs -I{} wget "${m3u8_url%index.m3u8}"{} 
	retval="$?"
	if [[ $retval -eq 0 ]]; then 
		cd ..
		tar cvfz m3u8.tar.gz m3u8/
		if [[ $? -ne 0 ]]; then echo "abort while tarballing m3u8/. ret:$retval" 2>&1 && exit 1; fi
		#\rm -r m3u8/
	else
		echo "abort while downloading m3u8s. ret:$retval" 2>&1
		exit 1
	fi
}

downloadarchive() {
	if ! command -v yt-dlp &> /dev/null; then
		echo "<the_command> could not be found" >&2 && exit 1
	fi
	# download archive by yt-dlp, cookie: ~/zancook
	yt-dlp --ignore-config --add-metadata --write-description -o "${filename}.%(ext)s" --cookies ~/zancook "$m3u8_url"
}

downloadm3u8
downloadcmts
downloadarchive
