#!/usr/bin/env zsh

if [[ $# -ne 1 ]]; then
  echo "Usage: $0 <manifest_file>" >&2
  exit 1
elif [[ ! -f "$1" ]]; then
  echo "Manifest file not found!" >&2
  exit 1
fi
manifest_file="$1"

last_cmt_url=$(jq -r 'last(.comment.list[].url)' $manifest_file)
last_spe_url=$(jq -r 'last(.special.list[].url)' $manifest_file)

url_prefix=${last_cmt_url%/*.json}
#spe_url_prefix=${last_cmt_url%/*.json}
#[[ "$url_prefix" == "$spe_url_prefix" ]] || {echo "throw exception" >&2 && exit 1;}

last_cmt_index=$(echo $last_cmt_url | sed -n  's/.*\/\(.*\)\.json$/\1/p')
last_spe_index=$(echo $last_spe_url | sed -n  's/.*\/specials_\(.*\)\.json$/\1/p')

# Download every comment/special json file which starts from index 1
retval=0
for i in {1..$last_cmt_index}; do
  wget -nc -q "${url_prefix}/${i}.json"
  retval=$((retval+?))
  echo -ne "\033[2KDownloaded: [CMT] $i\\r"
done
echo ""
for i in {1..$last_spe_index}; do
  wget -nc -q "${url_prefix}/specials_${i}.json"
  echo -ne "\033[2KDownloaded: [SPE] $i\\r"
  retval=$((retval+?))
done
echo -e "\nwget retval: $retval"
