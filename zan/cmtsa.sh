#!/usr/bin/env zsh

# for archive comments, does not have last index indicator. not 
# REF: https://vodcmt.zan-live.com/comment-archive/cee2d395018e24d91009cfd38e99fb84.index.json

if [[ $# -ne 1 ]]; then
  echo "Usage: $0 <manifest_file>" >&2
  exit 1
elif [[ ! -f "$1" ]]; then
  echo "Manifest file not found!" >&2
  exit 1
fi
manifest_file="$1"

cmt_urls=$(jq -r '.comments | keys_unsorted[]' $manifest_file)

# Download every comment/special json file which starts from index 1
retval=0
i=0
while read -r line; do
  i=$((i+1))
  wget -nc -q $line
  retval=$((retval+?))
  echo -ne "\033[2KDownloaded: [CMT] $i\\r"
done <<< $cmt_urls
echo ""
# TODO: multiple others jsons in an array
wget -nc -q $(jq -r '.others' $manifest_file)
echo -ne "\033[2KDownloaded: [SPE] 1\\r"
retval=$((retval+?))

echo -e "\nwget retval: $retval"
