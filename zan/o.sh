#!/usr/bin/env zsh

fine_last=450606
INTERVAL=45

while :; do
  for i in {`cat ../lastseg`..$fine_last}; do
    sem --jobs=12 ./f.sh $i 
  done
  sem --wait
  echo to $fine_last
  sleep $INTERVAL
  # under seg/
  current_last=$(ls -v index_8_*.ts?aka_me_session_id=* | tail -1 | sed -e 's/^index_8_//' -e 's/.ts?aka_me_session.*//')
  ./c.sh "$current_last"
  miss_count=$?
  if [[ $miss_count -eq 0 ]]; then
    fine_last=$current_last
  fi
done
