#!/usr/bin/env zsh

file="$1"
if [[ ! -f $file ]]; then
    2>&1 echo "File: \"$file\" inavailable. Abort."
    exit
fi

filename=${file%%.log}
#url=$(cat $file | grep -oP "(?<=Space url: )https://twitter.com/i/spaces/.*$")
username=$(cat $file | grep -P "Space info" | grep -oP "(?<=username\":\").*?(?=\")")
id=$(cat $file | grep -P "Space info" | grep -oP "(?<=id\":\").*?(?=\")")
title=$(cat $file | grep -P "Space info" | grep -oP "(?<=title\":\").*?(?=\")")
playlist_url=$(cat $file | grep -P "Space info" | grep -oP "(?<=playlist_url\":\").*?(?=\")")
ts=$(cat $file | grep -oP "(?<=created_at\":)[0-9]+(?=\,)")

#echo $username
#echo $id
#echo $title
#echo $ts
#echo $playlist_url

#twspace_dl -c ~/space_greensg_2023_0908/cookie.txt -f "$playlist_url" -o "m4a/${username}.space_${id}.title_${title}.${ts}.${filename}"
twspace_dl -c ../cookie.txt -f "$playlist_url" -o "m4a/${username}.space_${id}.title_${title}.${ts}.${filename}"
