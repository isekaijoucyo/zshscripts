#!/bin/bash

validate_integer_or_set_default () {
  # check if $1 is an unsigned integer, if not, print the default value
  [[ -z $1 ]] && return 1
  if [[ -z $2 ]]; then
    default_value=0
  else
    [[ $2 =~ ^[0-9]+$ ]] && default_value="$2" || default_value=0
  fi
  if [[ $1 =~ ^[0-9]+$ ]]; then
    echo $1
    return 0
  else
    echo $default_value
    return 0
  fi
}

if [[ ! -z $1 ]]; then
  dir_size=$(validate_integer_or_set_default "$1" 200)
  if [[ $? -ne 0 ]]; then
    exit 1
  fi
fi

dir_tosplit="seg"
dir_name="seg_split_"
bucket=$((($(find $dir_tosplit -maxdepth 1 -type f | wc -l) + ($dir_size - 1)) / $dir_size))
echo "bucket count: $bucket"
for i in `seq 1 $bucket`; do
    mkdir -p "$dir_name$i"
    # use ls -v natural sort order
    ls -v $dir_tosplit | head -n $dir_size | xargs -i mv -n "$dir_tosplit/""{}" "$dir_name$i"
    #find $dir_tosplit -maxdepth 1 -type f | sort -n | head -n $dir_size | xargs -i mv -n "{}" "$dir_name$i"
done
