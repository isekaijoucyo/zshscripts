#!/usr/bin/env zsh

LOGFILE="$HOME/checkytid_$(date +%Y-%m-%d).log"
TIMESTAMP_1MONTH_AGO="$(date -d '+1 month ago' +%s)"
TEMPFILE="$HOME/_checkytid.log"
\rm -vf $TEMPFILE

echo "[START]: [$(date +%Y-%m-%d)]"
echo "[START]: ($(date +%s))" | tee -a $LOGFILE

cnt=0
for f in *-*.mp4; do
	echo "DEBUG: Processing $f"
	# based on default yt-dlp output template
	# the last 11 alphanumeric/hyphen/underscore combination before the extesnion name
	ytid=$(echo $f | sed -nE 's/.*-([A-Za-z0-9_-]{11})\.mp4/\1/p');
	result=$(yt-dlp --simulate --print "%(duration>%H:%M:%S.%s)s %(upload_date>%Y-%m-%d)s %(id)s" -- "$ytid" 2>&1)
	if echo "$result" | grep -q "^ERROR"; then
		# the ytid video is inaccessible on youtube, keep this video in my backup instead.
		echo "${ytid} KEEP1" | tee -a $LOGFILE
	else
		result_arr=($=result)
		TIMESTAMP_YTID_VIDEO="$(date -d "$result_arr[2]" +%s)"
		if [[ $TIMESTAMP_YTID_VIDEO -gt $TIMESTAMP_1MONTH_AGO ]]; then
			# the ytid video is accessible, but within a month, roughly. Keep this video in my backup in case it is removed later on.
			echo "${ytid} KEEP2 [$result_arr[2]]"
			echo "${ytid} KEEP2" >> $LOGFILE
		else
			echo "${ytid} REMOVE [$result_arr[2]]"
			echo "${ytid} REMOVE" >> $LOGFILE
			echo -n "${ytid} " >> $TEMPFILE
		fi
	fi
	cnt=$((cnt+1))
done
echo "[END]: ($(date +%s)) Done [$(date +%Y-%m-%d)] Total: $cnt" | tee -a $LOGFILE
echo "Logfile: $LOGFILE"
